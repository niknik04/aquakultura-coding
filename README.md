#Сборка проекта на Gulp.

##1. Установка

Устанавливаем Gulp:
```
$ npm install gulp
```

Устанавливаем Gulp в папке проекта:
```
$ npm install gulp --save-dev
```

Устанавливаем все необходимые модули:
```
$ npm install gulp-watch gulp-myth gulp-sourcemaps gulp-csso gulp-sass gulp-imagemin imagemin-pngquant gulp-uglify gulp-concat --save-dev
```


##2. Работа со сборкой:

Все рабочие исходники лежат в папке /src

Для работы со стилями используется [SASS](http://sass-lang.com/). Все файлы лежат в папке /css/scss/blocks/ и инклюдятся в mian.scss в папке /css/scss/.

Скрипты собираются в /js/main.js из папки /js/assets/

##3. Возможные таски прописанные в gulpfile.js:

```
$ gulp watch
```
Отслеживает все изменения в файлах стилей и скриптов и собирает исходные /css/main.css и /js/main.js.


```
$ gulp dev
```
Запускает локальный сервер с модулем watch.


```
$ gulp all
```
Делает рабочую сборку для сервера верстки из /src в /static. Минимизируются стили и скрипты (подключение опционально), ужимаются и переносятся все изображения, переносятся html файлы.


```
$ gulp build
```
Минимизируются и переносятся в /static только стили и скрипты.