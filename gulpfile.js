﻿var gulp = require('gulp'), // Сообственно Gulp JS
    watch = require('gulp-watch'),
    myth = require('gulp-myth'), // Плагин для Myth - http://www.myth.io/
    sourcemaps = require('gulp-sourcemaps'), // Sourcemaps
    csso = require('gulp-csso'), // Минификация CSS
    sass = require('gulp-sass'),// SASS Компилятор
    imagemin = require('gulp-imagemin'),// Минификация изображений
    pngquant = require('imagemin-pngquant'),
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat');

gulp.task('scss', function() {
    gulp.src(['./src/css/scss/main.scss'])
        .pipe(sourcemaps.init())
            .pipe(sass({indentedSyntax: true, errLogToConsole: true}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./src/css'))
});
gulp.task('css-min', function() {
    gulp.src(['./src/css/main.css'])
        .pipe(concat('main.min.css'))
        .pipe(myth())
        .pipe(csso())
        .pipe(gulp.dest('./static/css'));

    gulp.src(['./src/css/*.css'])
        .pipe(gulp.dest('./static/css'))
    gulp.src(['./src/css/vendors/*.css'])
        .pipe(gulp.dest('./static/css/vendors'))
});

gulp.task('js', function() {
    gulp.src(['./src/js/assets/*.js'])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./src/js'));
});
gulp.task('js-min', function() {
    gulp.src(['./src/js/main.js'])
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./static/js'));
    gulp.src(['./src/js/**/*','!./src/js/assets/*'])
        .pipe(gulp.dest('./static/js'));
});

gulp.task('imgs', function () {
    gulp.src('./src/img/**/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./static/img'));
});

gulp.task('files', function () {
    gulp.src('./src/*.html')
        .pipe(gulp.dest('./static'));
    gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./static/fonts'));
});



// Watcher
gulp.task('watch', function() {
    gulp.watch('./src/css/scss/**/*.scss', ['scss']);
    gulp.watch('./src/js/assets/**/*.js', ['js']);
});

// Dev server
gulp.task('dev', ['watch'], function() {
    var express = require('express');
    var app = express();
    app.set('views', __dirname + '/src');
    app.use(express.logger('dev'));
    app.use(express.static(__dirname + '/src'));
    app.listen(9001);
});

// Build
gulp.task('build', ['css-min', 'js-min'], function() {
});

// Build All
gulp.task('all', ['css-min', 'js-min', 'imgs', 'files'], function() {
});

