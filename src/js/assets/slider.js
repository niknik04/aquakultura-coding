$.fn.slider = function(){
  var par = $(this);
  function updateContentByTab(idx) {
    par.find('.slider > div').removeClass('active').filter(':eq('+idx+')').addClass('active');
  }
  par.find('.prev').click(function(e){
    var idx = par.find('.slider > div.active').prev().index();
    if (par.find('.slider > div.active').is(':first-child'))
      idx = par.find('.slider > div').size()-1;
    updateContentByTab(idx);
    e.preventDefault();
  });
  par.find('.next').click(function(e){
    var idx = par.find('.slider > div.active').next().index();
    if (par.find('.slider > div.active').is(':last-child'))
      idx = 0;
    updateContentByTab(idx);
    e.preventDefault();
  });
};

$.fn.gallery = function(){
  $(this).each(function(){
    var par = $(this);
    function updateContentByTab(idx) {
      par.find('.previews span').removeClass('active').filter(':eq('+idx+')').addClass('active');
      par.find('.bigImg img').removeClass('active').filter(':eq('+idx+')').addClass('active');
    }
    par.find('.previews span').click(function(e){
      updateContentByTab($(this).index());
      e.preventDefault();
    });
  })
};

