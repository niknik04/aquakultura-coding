$.fn.tabs = function(isCall){
  var par = $(this);
  function updateContentByTab(obj) {
    par.find('li').removeClass('active').filter(obj).addClass('active');
    $(par.data('tabs')).find('> div').removeClass('active').filter(':eq('+obj.index()+')').addClass('active');
  }
  par.find('li').click(function(e){
    updateContentByTab($(this));
    return false;
  });
};



$.fn.filters = function(){
  var par = $(this);
  function updateContentByTab(obj) {
    par.find('li').removeClass('active').filter(obj).addClass('active');
    if (obj.data('filter'))
      $(par.data('src')).find('> .project').slideUp().filter('[data-filter='+obj.data('filter')+']').stop().slideDown();
    else
      $(par.data('src')).find('> .project').slideDown();
  }
  par.find('li').click(function(e){
    updateContentByTab($(this));
    return false;
  });
};



$.fn.tableTabs = function(isCall){
  function updateContentByTab(obj) {
    tableTabIndex = obj.closest('td').index();
    var par = obj.closest('tr');
    par.find('td span.tab.active').find('a').css('left',0);
    par.find('td span.tab').removeClass('active').filter(obj).addClass('active');
    $('#production_table-tabs_content').find('> div').removeClass('active').filter(':eq('+(tableTabIndex-1)+')').addClass('active');
  }
  $(this).find('table tr td .tab').click(function (e) {
    updateContentByTab($(this));
    e.preventDefault()
  });
}