$.fn.dialog = function( options ) {
  var win = $(this),
      form = win.find('form');

  var oldWin = $('div.popup.active');
  if(oldWin.attr('id')) {
    if(oldWin.attr('id') != win.attr('id')) {
      oldWin.dialog({
        destroy: true,
        keepFader: true
      });
    }
  }

  if (win.attr('id') == 'complexLines') {
    win.find('content').html()
  }

  if (!options || options.content) {
    $('body').addClass('overlay');
    $('#fader').fadeIn();
    win.addClass('active');
    if (options) {
      win.find('.content').html(options.content);
      win.find('.close').click(function(){
        $('div.popup.active').dialog({destroy: true});
        return false;
      });
      imagesLoaded( win, function( instance ) {
        var winTopPos = $(window).height()/2-win.height()/2-25
        if (winTopPos < 20) winTopPos = 20;
        win.css('top',winTopPos);
      });
      $(window).resize(function(){
        var winTopPos = $(window).height()/2-win.height()/2-25
        if (winTopPos < 10) winTopPos = 10;
        win.css('top',winTopPos);
      })

    }

    if (form.size()) {
      //form.initForm();
    }
  }


  if (options)
    if (options.destroy) {
      if (form.size()) {
        //form.initForm({destroy: true});
      }

      win.removeClass('active').find();
      $('body').removeClass('overlay');
      if (!options.keepFader) {
        $('#fader').fadeOut();
      }
    }
};
