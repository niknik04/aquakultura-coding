//var interval = 7000;
var isMobile = 0;

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
  isMobile = 1;
  document.documentElement.className += 'mobile';
}

var pageId = '', subPageID = '';
if (location.href.indexOf('#')>0) {
  if (location.href.lastIndexOf('/')>location.href.indexOf('#')) {
    pageId = location.href.substring(location.href.lastIndexOf('#') + 1, location.href.lastIndexOf('/'));
    subPageID = location.href.substring(location.href.lastIndexOf('/') + 1);
  } else {
    pageId = location.href.substring(location.href.lastIndexOf('#') + 1);
  }
}



$(document).ready(function() {

  // tabs init
  $('section.complex ul.tabs').tabs();
  $('section.services ul.tabs').tabs();
  $('section.main.complex-line ul.tabs').tabs();

  $('section.main .big-table .table-content').tableTabs();

  $('#top .filter ul').filters();
  // tabs init

  // sliders and carousel init
  $('section.main.success .carousel').slider();

  $('section.main .content .gallery').gallery();
  // sliders and carousel init

  if ($('section.main .big-table').size())
    $('section.main .big-table').tableInit()


  $('section.complex-line .showDialog').click(function(){
    $('#'+$(this).data('dialog')).dialog({content: $(this).closest('.line-img').html()});
    return false
  });
  $('header .showDialog, section.main.services a.showDialog').click(function(){
    $('#'+$(this).data('dialog')).dialog();
  });
  $('#fader .backdoor, div.popup .close').click(function(){
    $('div.popup.active').dialog({destroy: true});
  });

  if ($(".fancybox-thumb").size())
    $(".fancybox-thumb").fancybox({
      prevEffect	: 'none',
      nextEffect	: 'none',
      helpers	: {
        title	: {
          type: 'outside'
        },
        thumbs	: {
          width	: 50,
          height	: 70
        }
      }
    });

  $('#fader .popup form').submit(function(){
    $(this).hide().parent().find('.success').show();
    return false;
  });
});

var tableTabIndex = 1;

$.fn.tableInit = function(){
  var tablesHolder = $(this);

  // table hovers
    tablesHolder.find('.table-content table tr td .tab a').each(function(){
    $(this).parent().css({
      'width': $(this).width()+18,
      'padding': '0 4px'
    });
    $(this).hover(
        function(e){
          $(this).addClass('hover');
          return false
        },
        function(e){
          $(this).removeClass('hover');
        }
    );
  })
  tablesHolder.find('table tr').hover(
      function(){
        var idx = $(this).index();
        $(this).closest('.big-table').find('table').each(function(){
          $(this).find('tr:eq('+idx+')').addClass('hover');
        })
      },
      function(){
        var idx = $(this).index();
        $(this).closest('.big-table').find('table').each(function(){
          $(this).find('tr:eq('+idx+')').removeClass('hover');
        })
      }
  );
  // table hovers

  // table resize
  $('section.main.production .more-table a').click( function(){
    tablesHolder.find('> div').animate({'max-height':tablesHolder.find('table').height()},500)
    $(this).parent().hide();
    return false
  });
  // table resize

  // drag table
  tablesHolder.find('.table-content table').mousedown(function(){
    $(this).addClass('grabbed');
  });
  tablesHolder.find('.table-content table').mouseup(function(){
    $(this).removeClass('grabbed');
  });

  tablesHolder.find('.table-content table').draggable({
    axis: "x",
    drag: function(e,ui) {
      var obj = $(this);
      if (ui.position.left < 0) {
        tablesHolder.addClass('scrolled');
      } else {
        tablesHolder.removeClass('scrolled');
      }
      if (ui.position.left < -obj.width()+obj.parent().width()+62) {
        tablesHolder.addClass('end');
      } else {
        tablesHolder.removeClass('end');
      }
      if (obj.find('td:eq('+tableTabIndex+') .tab').offset().left < 33) {
        obj.find('td:eq('+tableTabIndex+') .tab').addClass('fixed');
        obj.find('td:eq('+tableTabIndex+') .tab a').css('left',-ui.position.left-obj.find('td:eq('+tableTabIndex+') .tab').position().left+13)
      } else {
        obj.find('td:eq('+tableTabIndex+') .tab').removeClass('fixed');
        obj.find('td:eq('+tableTabIndex+') .tab a').css('left',0)
      }
    },
    stop: function(e,ui) {
      var obj = tablesHolder.find('.table-content table');
      if (ui.position.left > 0) {
        obj.animate({'left':0},500);
      } else if (ui.position.left < -obj.width()+obj.parent().width()+62) {
        obj.animate({'left':-obj.width()+obj.parent().width()+62},500);
        obj.find('td:eq('+tableTabIndex+') .tab a').animate({'left':(obj.width()-obj.parent().width())-(obj.find('td:eq('+tableTabIndex+') .tab').position().left-13)-62},500);
      }
    }
  });
  //drag table

}